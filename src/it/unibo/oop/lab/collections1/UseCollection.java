package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 *
 */
public final class UseCollection {

	private static final int CAPACITY = 1000;
	private static final int MAX_NUMBERS = 100_000;

	private UseCollection() {
	}

	/**
	 * @param s
	 *            unused
	 */
	public static void main(final String... s) {
		/*
		 * 1) Create a new ArrayList<Integer>, and populate it with the numbers
		 * from 1000 (included) to 2000 (excluded).
		 */
		final ArrayList<Integer> listInt = new ArrayList<>();
		listInt.ensureCapacity(CAPACITY);
		for (int i = 1000; i < 2000; i++) {
			listInt.add(i);
		}

		/*
		 * 2) Create a new LinkedList<Integer> and, in a single line of code
		 * without using any looping construct (for, while), populate it with
		 * the same contents of the list of point 1.
		 */

		final List<Integer> listLinked= new LinkedList<>(listInt);
		/*
		 * 3) Using "set" and "get" and "size" methods, swap the first and last
		 * element of the first list. You can not use any "magic number".
		 * (Suggestion: use a temporary variable)
		 */
		final int temp = listInt.get(0);
		listInt.set(0, listInt.get(listInt.size() - 1));
		listInt.set(listInt.size() - 1, temp);
		/*
		 * 4) Using a single for-each, print the contents of the arraylist.
		 */
		for (final int integer : listInt) {
			System.out.println(integer);
		}
		/*
		 * 5) Measure the performance of inserting new elements in the head of
		 * the collection: measure the time required to add 100.000 elements as
		 * first element of the collection for both ArrayList and LinkedList,
		 * using the previous lists. In order to measure times, use as example
		 * TestPerformance.java.
		 */
		System.out.println(timeToInsert(listInt, MAX_NUMBERS) + " vs " + timeToInsert(listLinked, MAX_NUMBERS));
		/*
		 * 6) Measure the performance of reading 1000 times an element whose
		 * position is in the middle of the collection for both ArrayList and
		 * LinkedList, using the collections of point 5. In order to measure
		 * times, use as example TestPerformance.java.
		 */
		System.out.println(timeToRead(listInt) + " vs " + timeToRead(listLinked) );
		/*
		 * 7) Build a new Map that associates to each continent's name its
		 * population:
		 *
		 * Africa -> 1,110,635,000
		 *
		 * Americas -> 972,005,000
		 *
		 * Antarctica -> 0
		 *
		 * Asia -> 4,298,723,000
		 *
		 * Europe -> 742,452,000
		 *
		 * Oceania -> 38,304,000
		 */
		final Map<String,Long> worldPop = new HashMap<>();
		worldPop.put("Africa", 1_110_635_000L);
		worldPop.put("Americas", 972005000l);
		worldPop.put("Antartica", 0l);
		worldPop.put("Asia", 4298723000l);
		worldPop.put("Europe", 742452000l);
		worldPop.put("Oceania", 38304000l);
		/*
		 * 8) Compute the population of the world
		 */
		long sum=0;
		for (final String str : worldPop.keySet()) {
			sum+=worldPop.get(str);
		}
		System.out.println(sum);
	}

	private static long timeToInsert(final List<Integer> target, final int numElements) {
		final long time = System.nanoTime();
		/*
		 * Run the benchmark
		 */
		for (int i = 1; i <= numElements; i++) {
			target.add(0, i);
		}
		/*
		 * Compute the time and print result
		 */
		return System.nanoTime()-time;
	}

	private static long timeToRead(final List<Integer> target) {
		final long time = System.nanoTime();
		/*
		 * Run the benchmark
		 */
		int temp;
		for (int i = 1; i <= CAPACITY; i++) {
			temp=target.get(target.size()/2);
		}
		/*
		 * Compute the time and print result
		 */
		return System.nanoTime()-time;
	}
}
