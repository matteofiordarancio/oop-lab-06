package it.unibo.oop.lab.exception2;

/**
 * Class modeling a BankAccount with strict policies: getting money is allowed
 * only with enough founds, and there are also a limited number of free ATM
 * transaction (this number is provided as a input in the constructor).
 *
 */
public class StrictBankAccount implements BankAccount {

	private final int usrID;
	private double balance;
	private int nTransactions;
	private final int nMaxATMTransactions;
	private static final double ATM_TRANSACTION_FEE = 1;
	private static final double MANAGEMENT_FEE = 5;
	private static final double TRANSACTION_FEE = 0.1;

	/**
	 *
	 * @param usrID
	 *            user id
	 * @param balance
	 *            initial balance
	 * @param nMaxATMTransactions
	 *            max no of ATM transactions allowed
	 */
	public StrictBankAccount(final int usrID, final double balance, final int nMaxATMTransactions) {
		this.usrID = usrID;
		this.balance = balance;
		this.nMaxATMTransactions = nMaxATMTransactions;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void deposit(final int usrID, final double amount) {
		if (checkUser(usrID)) {
			balance += amount;
			incTransactions();
		}
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void withdraw(final int usrID, final double amount) {
		if (checkUser(usrID) && isWithdrawAllowed(amount)) {
			balance -= amount;
			incTransactions();
		}
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void depositFromATM(final int usrID, final double amount) {
		if (isTransactionPossibile()) {
			deposit(usrID, amount - StrictBankAccount.ATM_TRANSACTION_FEE);
			nTransactions++;
		}
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void withdrawFromATM(final int usrID, final double amount) {
		if (isTransactionPossibile()) {
			withdraw(usrID, amount + StrictBankAccount.ATM_TRANSACTION_FEE);
		}
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public double getBalance() {
		return balance;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public int getNTransactions() {
		return nTransactions;
	}

	/**
	 *
	 * @param usrID
	 *            id of the user related to these fees
	 */
	public void computeManagementFees(final int usrID) {
		final double feeAmount = MANAGEMENT_FEE + nTransactions * StrictBankAccount.TRANSACTION_FEE;
		if (checkUser(usrID) && isWithdrawAllowed(feeAmount)) {
			balance -= MANAGEMENT_FEE + nTransactions * StrictBankAccount.TRANSACTION_FEE;
			nTransactions = 0;
		}
	}

	private boolean checkUser(final int id) {
		if (usrID == id) {
			return true;
		}
		throw new WrongAccountHolderException();
	}

	private boolean isWithdrawAllowed(final double amount) {
		if (balance > amount) {
			return true;
		}else {
			throw new NotEnoughFundsException();
		}
	}

	private boolean isTransactionPossibile() {
		if (nTransactions < nMaxATMTransactions) {
			return true;
		}
		throw new TransactionOverQuotaException();
	}

	private void incTransactions() {
		nTransactions++;
	}

}