package it.unibo.oop.lab.exception2;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 *
 */
public class TestStrictBankAccount extends TestCase{

	/**
	 * Used to test Exceptions on {@link StrictBankAccount}.
	 */
	@Test
	public void testBankOperations() {
		/*
		 * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
		 * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
		 *
		 * 2) Effetture un numero di operazioni a piacere per verificare che le
		 * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
		 * presenza di un id utente errato, oppure al superamento del numero di
		 * operazioni ATM gratuite.
		 */
		final BankAccount ah1 =  new StrictBankAccount(0, 100.5, 10);
		final BankAccount ah2 =  new StrictBankAccount(1, 1000.5, 1);

		ah2.depositFromATM(1, 1000);
		try {
			ah2.depositFromATM(1, 0.5);
			fail();
		} catch (final TransactionOverQuotaException e) {

		}
		try {
			ah1.depositFromATM(5, 0.5);
			fail();
		} catch (final WrongAccountHolderException e) {

		}
	}
}
