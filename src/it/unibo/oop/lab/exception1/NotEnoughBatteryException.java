package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException{

	private static final long serialVersionUID = 1L;


	public NotEnoughBatteryException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotEnoughBatteryException(final String message, final Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotEnoughBatteryException(final String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	public NotEnoughBatteryException(final Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
